require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM01")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-230")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-4")
epicsEnvSet("EVR_NAME",       "PBI-BPM01:Ctrl-EVR-201:")
epicsEnvSet("SYSTEM1_PREFIX", "DTL-010:PBI-BPM-006:")
epicsEnvSet("SYSTEM1_NAME",   "DTL 010 BPM 06")
epicsEnvSet("SYSTEM2_PREFIX", "DTL-020:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "DTL 020 BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

